<?php
/**
 * Trafero
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   Lang
 * @license   MIT
 * @link      https://bitbucket.org/sunkan/trafero
 * @author    Denis Smetannikov <denis@jbzoo.com>
 * @author    Andreas Sundqvist <andreas@forme.se>
 */

namespace Trafero;

use Actus\Path;
use Codex\Data;
use Codex\Ini;
use Codex\JSON;
use Codex\PHPArray;

/**
 * Class Translate
 * @package Trafero
 */
class Translate
{
    const DEFAULT_FORMAT = 'php';
    const DEFAULT_MODULE = '_global';

    /**
     * @var string
     */
    protected $_code;

    /**
     * @var Path
     */
    protected $_path;

    /**
     * @var Data
     */
    protected $_storage;

    /**
     * @param $code
     */
    public function __construct(Path $path)
    {
        $this->_path = $path;
        $this->_storage = new Data();
    }

    /**
     * @param string $code 2 letter language code
     */
    public function setLang($code)
    {
        $this->_code = self::_cleanCode($code);
    }

    /**
     * @param string|array $path
     * @param string|null  $module
     * @param string       $format
     * @return bool
     *
     * @throws Exception
     * @throws \Actus\Exception
     */
    public function load($path, $module = null, $format = self::DEFAULT_FORMAT)
    {
        $module = $this->_cleanMessage($module);
        $format = $this->_clean($format);
        $path = realpath($path);

        if ($path && !$this->_storage->get($module . '.' . $format)) {
            $this->_path->add($path, $module);

            if (!in_array($format, ['ini', 'php', 'json'], true)) {
                throw new Exception('Undefined format: ' . $format);
            }

            $this->_storage->set($module, $format);
            if ($module !== self::DEFAULT_MODULE) {
                $this->load($path, self::DEFAULT_MODULE, $format);
            }

            return true;
        }

        return false;
    }

    /**
     * Set multiple paths
     * @param array $paths
     */
    public function setPaths(array $paths)
    {
        foreach ($paths as $info) {
            $this->load($info['path'], $info['module'] ?: null, $info['format'] ?: self::DEFAULT_FORMAT);
        }
    }

    /**
     * @param string $origMessage
     * @return string
     */
    public function translate($origMessage, array $args = [])
    {
        if (false === strpos($origMessage, '.')) {
            $module = self::DEFAULT_MODULE;
            $message = $origMessage;
        } else {
            list($module, $message) = explode('.', $origMessage, 2);
        }

        $module = $this->_clean($module);
        $message = $this->_cleanMessage($message);
        $format = $this->_storage->get($module, self::DEFAULT_FORMAT);

        $list = $this->_listFactory($module, $format);
        $text = $list->get($message);
        if ($text) {
            $text = (string) $text;
        } else {
            $listDefault = $this->_listFactory(self::DEFAULT_MODULE, $format);

            $text = (string) $listDefault->get($message, $origMessage);
        }

        if (count($args) && strpos($text, '{') !== false) {
            $replaceList = [];
            foreach ($args as $key => $value) {
                $replaceList['{' . $key . '}'] = $value;
            }
            return str_replace(array_keys($replaceList), array_values($replaceList), $text);
        }
        return $text;
    }

    /**
     * @param string $code
     * @return string
     * @throws Exception
     */
    protected static function _cleanCode($code)
    {
        $code = preg_replace('#[^a-zA-Z]#', '', $code);
        $code = trim(strtolower($code));
        if (strlen($code) !== 2) {
            throw new Exception('Lanuage code is only two chars: ' . $code);
        }

        return $code;
    }

    /**
     * @param string $message
     * @return string
     */
    protected function _cleanMessage($message)
    {
        $message = preg_replace('#[^a-z0-9_\-\.]#i', '', $message);
        $message = $this->_clean($message);
        $message = $message ?: self::DEFAULT_MODULE;

        return $message;
    }

    /**
     * @param string $string
     * @return string
     */
    protected function _clean($string)
    {
        return trim(strtolower($string));
    }

    /**
     * @param string $module
     * @param string $format
     * @return Data
     */
    protected function _listFactory($module, $format)
    {
        $key = $module . '.' . $format;
        if ($list = $this->_storage->get($key)) {
            return $list;
        }

        if ($module === self::DEFAULT_MODULE) {
            $path = $module . ':langs/' . $this->_code . '.' . $format;
        } else {
            $path = $module . ':langs/' . $this->_code . '.' . $module . '.' . $format;
        }
        $listPath = $this->_path->get($path);

        if ('php' === $format) {
            $list = new PHPArray($listPath);
        } elseif ('json' === $format) {
            $list = new JSON($listPath);
        } elseif ('ini' === $format) {
            $list = new Ini($listPath);
        } else {
            $list = new Data([]);
        }

        $this->_storage->set($key, $list);

        return $list;
    }
}
